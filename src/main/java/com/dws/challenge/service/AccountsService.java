package com.dws.challenge.service;

import com.dws.challenge.domain.Account;
import com.dws.challenge.repository.AccountsRepository;
import lombok.Getter;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountsService {

  @Getter
  private final AccountsRepository accountsRepository;
  
  @Autowired
  private NotificationService notificationService;

  @Autowired
  public AccountsService(AccountsRepository accountsRepository) {
    this.accountsRepository = accountsRepository;
  }

  public void createAccount(Account account) {
    this.accountsRepository.createAccount(account);
  }

  public Account getAccount(String accountId) {
    return this.accountsRepository.getAccount(accountId);
  }
  
  public String transferAmount(String accountFromId, String accountToId, BigDecimal amount) {
	    
	  Account accountFrom = getAccount(accountFromId);
	  Account accountTo = getAccount(accountToId);
	  synchronized (accountFrom) {
		  
		  synchronized (accountTo) {
			if(accountFrom.getBalance().subtract(amount).signum() >= 0) {
				accountFrom.setBalance(accountFrom.getBalance().subtract(amount));
				accountTo.setBalance(accountTo.getBalance().add(amount));
				notificationService.notifyAboutTransfer(accountFrom, "Your account got debited "+ amount+" and transfered to account Id - "+ accountTo.getAccountId());
				notificationService.notifyAboutTransfer(accountTo, "Your account got credited with "+ amount+" from account Id - "+ accountFrom.getAccountId());
				return "Amount transferred successfuly";
			}else {
				return "Insufficient Balance in account - "+accountFrom.getAccountId();
			}
		  }		
	  }
  }
  
}
